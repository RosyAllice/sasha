
function closeAndOpen(elem){
    if(elem.id == "listening")
        var array = document.getElementsByClassName("listening_section");
    if(elem.id == "devices")
        var array = document.getElementsByClassName("menu_section");
    var arrayHeight = [];
    arrayHeight[0] = parseStyle(getStyle(array[0]).height);
    arrayHeight[1] = parseStyle(getStyle(array[1]).height);
    if(arrayHeight[0] > arrayHeight[1]){
        array[1].style.height = 65 + "vh";
        array[1].classList.remove("hovered");
        array[1].classList.add("scrolled");
        array[0].scrollTop = 0;
        array[0].style.height = 10 + "vh";
        array[0].classList.add("hovered");
        array[0].classList.remove("scrolled");
    }
    else if(arrayHeight[1] > arrayHeight[0]){
        array[0].style.height = 65 + "vh";
        array[0].classList.remove("hovered");
        array[0].classList.add("scrolled");
        array[1].scrollTop = 0;
        array[1].style.height = 10 + "vh";
        array[1].classList.add("hovered");
        array[1].classList.remove("scrolled");
    }
    
}

function parseStyle(style){
    style = Number.parseInt(style.slice(0,-2));
    return style;
}

function getStyle(elem){
if(window.getComputedStyle) return getComputedStyle(elem, null);
else return elem.currentStyle;
}